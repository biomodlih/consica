This is a ConsICA project - calculation of consensus independent component analysis (ICA) and reporting the results <br>

<b>LibICA.r</b> contains the main functions:
<ul>
<li> runICA() - core function to calculate ICA </li> 
<li> getGenesICA() - get top contibuting features (genes)</li>
<li> getGO() - performs functional enrichment analysis of top-contributing genes</li>
<li> saveGO() - saves results of functional enrichment analysis and top-contributing genes to text files</li>
<li> saveGenes() - saves only top-contributing genes into text files</li>
<li> saveReport() - saves PDF report describing ICA results</li>
</ul>
<br>
<b>enrichGO.r</b> a warpup around topGO package for GO functional enrichment analysis
<br>
<b>violinplot.r</b> violin plots (analogouos to boxplots)