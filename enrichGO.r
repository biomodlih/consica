###############################################################################
## enrichGOens - warpup for topGO package: enrichment analysis of GO-terms
## based on Ensembl IDs
##-----------------------------------------------------------------------------
## genes   - vector with list of ENSEBML IDs (character)
## fdr     - vector of FDR for each gene (numeric)
## fc      - vector of logFC for each gene (numeric)
## thr.fdr - significance threshold for FDR (numeric)
## thr.fc  - significance threshold for absolute logFC (numeric)
## db      - name of GO database: "BP","MF","CC" (character)
## genome  - R-package for genome annotation used. For human - 'org.Hs.eg.db' (character)
## do.sort - if TRUE - resulted functions sorted by p-value (logical)
## randomFraction - for testing only, the fraction of the genes to be randomized (numeric)
##-----------------------------------------------------------------------------
## (c)GNU GPL P.Nazarov 2014. petr.nazarov[at]crp-sante.lu
###############################################################################

## TESTING & DEBUGGING
if(FALSE){ 
	genes=NGS$anno$id
	fdr=Res$SCC$edgeR$FDR
	ntop = 1000
	thr.fdr=0.01
	thr.fc=NA
	db="BP"
	genome="org.Hs.eg.db"
	do.sort=FALSE
	randomFraction=0*0.2
}

## FUNCTION IMPLEMENTATION
enrichGOens = function(genes,fdr=NULL,fc=NULL,ntop=NA,thr.fdr=0.05,thr.fc=NA,db="BP",genome="org.Hs.eg.db",do.sort=TRUE,randomFraction=0,return.genes=FALSE){
	## load libraries
	if (!require(genome,character.only=TRUE)){
		cat("MESSAGE enrichGO: '",genome,"' package is not found. Installing...\n",sep="")
		source("http://bioconductor.org/biocLite.R")
		biocLite(genome)
		library(genome,character.only=TRUE)
	}
	if (!require("topGO")){
		cat("MESSAGE enrichGO: ' topGO ' package is not found. Installing...\n")
		source("http://bioconductor.org/biocLite.R")
		biocLite("topGO")
		library("topGO")
	}
	
	if (is.na(ntop)) cat("ntop is NA => using FDR (FC) as limits\n")
	
	if (!exists("sortDataFrame")) source("http://sablab.net/scripts/sortDataFrame.r")
	
	## create score depending on threshold and paradigm
	if (is.null(fc) | is.na(thr.fc)){
		score = (-log10(fdr))
		if (is.na(ntop)){
			score[fdr>=thr.fdr]=0
		}else{
			score[sort(score,index.return=TRUE,decreasing=TRUE)$ix[-(1:ntop)]]=0
		}
	}else{
		score = (-log10(fdr)*abs(fc))
		if (is.na(ntop)){
			score[fdr>=thr.fdr | abs(fc)<=thr.fc]=0
		}else{
			score[sort(score,index.return=TRUE,decreasing=TRUE)$ix[-(1:ntop)]]=0
		}
	}
	names(score)=genes
	
	## add randomness if required, to test stability
	if (randomFraction>0){ 
		## define remove probability: low score have more chances
		prob1 = 1/(1+score)
		prob1[is.na(prob1)]=0
		prob1[score == 0] = 0
		## define add probability: high score has more chances
		prob2 = -log10(fdr)*abs(fc)
		prob2[is.na(prob2)]=0
		prob2[score > 0] = 0
		## add and remove
		n=round(sum(score>0)*randomFraction)
		score[sample(1:length(genes),n,prob=prob2)]=1+rexp(n,1/mean(score[score>0]))
		score[sample(1:length(genes),n,prob=prob1)]=0
	}
	
	
	## prepare gene categories and score
	myGO2genes <- annFUN.org(whichOnto=db, mapping = genome, ID = "ensembl")
	length(myGO2genes)
	
	## create topGOdata object
	SelectScore = function(sc){return(sc>0)} ## simple function for significance
	GOdata = new("topGOdata",  ##constructor
							ontology = db,
							allGenes = score,
							geneSelectionFun = SelectScore,
							annot = annFUN.GO2genes,
							GO2genes = myGO2genes)
							#annot = annFUN.org,
							#mapping = genome) 
							#ID = "ensembl")
	## run testing
	resFisher = runTest(GOdata, algorithm = "classic", statistic = "fisher")
	## transform results into a table
	enrichRes = GenTable(GOdata, classicFisher = resFisher, 
						ranksOf = "classicFisher",topNodes = length(resFisher@score))
	enrichRes$classicFisher[grep("<",enrichRes$classicFisher)] = "1e-31"
	enrichRes$classicFisher = as.double(enrichRes$classicFisher)
	enrichRes$FDR = p.adjust(enrichRes$classicFisher,"fdr")
	enrichRes$Score = -log10(enrichRes$FDR)
	## by default sorted by p-value. If needed - sort by ID
	if (!do.sort) enrichRes = sortDataFrame(enrichRes,"GO.ID") ## remove sorting
	rownames(enrichRes) = enrichRes$GO.ID
	
	
	if (return.genes){
		print("GO2gene annotation");flush.console()
		sig.genes = genes[score>0]
		g2g = matrix("",nrow = length(names(GOdata@graph@nodeData@data)),ncol=4)
		rownames(g2g) =  names(GOdata@graph@nodeData@data)
		colnames(g2g) = c("n.sig","n.all","genes.sig","genes.all")
		for (i in 1:nrow(g2g)) {
			x = ls(GOdata@graph@nodeData@data[[i]][[1]])
			x.sig = x[x%in%sig.genes]
			g2g[i,"genes.all"] = paste(x,collapse=",")
			g2g[i,"genes.sig"] = paste(x.sig,collapse=",")
			g2g[i,"n.all"] = length(x)
			g2g[i,"n.sig"] = length(x.sig)
			if(i%%1000 == 0) print(i)
		}
		enrichRes$n.sig = as.integer(g2g[enrichRes$GO.ID,"n.sig"])
		enrichRes$n.all = as.integer(g2g[enrichRes$GO.ID,"n.all"])
		enrichRes$genes.sig = g2g[enrichRes$GO.ID,"genes.sig"]
		enrichRes$genes.all = g2g[enrichRes$GO.ID,"genes.all"]
	}
	return(enrichRes)
	#write.table(enrichRes, file = sprintf("enrichRes_%s.txt",db),sep="\t",quote=F,row.names=F)
}
#######################################################################################
if (FALSE){
	genes = entrez.genes
	fdr =  as.integer(W[,icomp]<quantile(W,0.9))
	fc = NULL
	ntop=NA
	thr.fdr=0.05
	thr.fc=NA
	db="BP"
	genome="org.Hs.eg.db"
	do.sort=TRUE
	randomFraction=0
	return.genes=FALSE
	id ="entrez"# c("entrez", "alias", "ensembl", "symbol", "genename", "unigene")
}
## Gene Symbol
enrichGO = function(genes,
					fdr=NULL,fc=NULL,ntop=NA,thr.fdr=0.05,thr.fc=NA,
					db="BP",
					genome="org.Hs.eg.db", 
					id = c("entrez", "alias", "ensembl", "symbol", "genename", "unigene"),
					algorithm = "weight", ## before 2019-02 was "classic"
					do.sort=TRUE,randomFraction=0,return.genes=FALSE)
{
	## load libraries
	if (!require(genome,character.only=TRUE)){
		cat("MESSAGE enrichGO: '",genome,"' package is not found. Installing...\n",sep="")
		source("http://bioconductor.org/biocLite.R")
		biocLite(genome)
		library(genome,character.only=TRUE)
	}
	if (!require("topGO")){
		cat("MESSAGE enrichGO: ' topGO ' package is not found. Installing...\n")
		source("http://bioconductor.org/biocLite.R")
		biocLite("topGO")
		library("topGO")
	}
	
	if (is.na(ntop)) cat("ntop is NA => using FDR (FC) as limits\n")
	
	if (!exists("sortDataFrame")) source("http://sablab.net/scripts/sortDataFrame.r")
	
	## create score depending on threshold and paradigm
	if (is.null(fc) | is.na(thr.fc)){
		score = (-log10(fdr))
		if (is.na(ntop)){
			score[fdr>=thr.fdr]=0
		}else{
			score[sort(score,index.return=TRUE,decreasing=TRUE)$ix[-(1:ntop)]]=0
		}
	}else{
		score = (-log10(fdr)*abs(fc))
		if (is.na(ntop)){
			score[fdr>=thr.fdr | abs(fc)<=thr.fc]=0
		}else{
			score[sort(score,index.return=TRUE,decreasing=TRUE)$ix[-(1:ntop)]]=0
		}
	}
	names(score)=genes
	
	cat("Significant:",sum(score>0),"\n")
	## prepare gene categories and score
	
	if (length(id)>1){ ## if so - find the best, i.e. the one which is overrepresented :)
		common = integer(length(id))
		names(common) = id
		for ( i in 1:length(id)){
			myGO2genes <- annFUN.org(whichOnto=db, mapping = genome, ID = id[i])
			common[i] = sum(genes %in% unique(unlist(myGO2genes)))
		}
		print(common)
		id = names(which.max(common))
		cat("Checking DB. The best overlap with [",id,"] gene IDs:",max(common),"\n")
	}
	myGO2genes <- annFUN.org(whichOnto=db, mapping = genome, ID = id)
	
	## create topGOdata object
	SelectScore = function(sc){return(sc>0)} ## simple function for significance
	GOdata = new("topGOdata",  ##constructor
							ontology = db,
							allGenes = score,
							geneSelectionFun = SelectScore,
							annot = annFUN.GO2genes,
							GO2genes = myGO2genes)
	## run testing
	resFisher = runTest(GOdata, algorithm = algorithm, statistic = "fisher")
	## transform results into a table
	enrichRes = GenTable(GOdata, classicFisher = resFisher, 
						ranksOf = "classicFisher",topNodes = length(resFisher@score))
	enrichRes$classicFisher[grep("<",enrichRes$classicFisher)] = "1e-31"
	enrichRes$classicFisher = as.double(enrichRes$classicFisher)
	enrichRes$FDR = p.adjust(enrichRes$classicFisher,"fdr")
	enrichRes$Score = -log10(enrichRes$FDR)
	## by default sorted by p-value. If needed - sort by ID
	if (!do.sort) enrichRes = sortDataFrame(enrichRes,"GO.ID") ## remove sorting
	rownames(enrichRes) = enrichRes$GO.ID
	
	if (return.genes){
		print("GO2gene annotation");flush.console()
		sig.genes = genes[score>0]
		g2g = matrix("",nrow = length(names(GOdata@graph@nodeData@data)),ncol=2)
		rownames(g2g) =  names(GOdata@graph@nodeData@data)
		colnames(g2g) = c("genes.all","genes.sig")
		for (i in 1:nrow(g2g)) {
			x = ls(GOdata@graph@nodeData@data[[i]][[1]])
			x.sig = x[x%in%sig.genes]
			g2g[i,"genes.all"] = paste(x,collapse=",")
			g2g[i,"genes.sig"] = paste(x.sig,collapse=",")
			if(i%%1000 == 0) print(i)
		}
		enrichRes$genes.sig = g2g[enrichRes$GO.ID,"genes.sig"]
		enrichRes$genes.all = g2g[enrichRes$GO.ID,"genes.all"]
	}
	return(enrichRes)
	#write.table(enrichRes, file = sprintf("enrichRes_%s.txt",db),sep="\t",quote=F,row.names=F)
}
#######################################################################################
